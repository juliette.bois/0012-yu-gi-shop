import AuthService from '../src/services/AuthService'
import jwtDecode from 'jwt-decode'

export default {
	state: {
		token: AuthService.getToken(),
		isAuth: AuthService.getToken() !== null
	},
	actions: {
		login ({commit}, credentials) {
			return AuthService.login(credentials)
				.then(() => {
					commit('LOGIN_SUCCESS')
				})
				.catch((error) => {
					commit('LOGIN_FAILURE')
					throw new Error(error)
				})
		},
		register ({commit}, credentials) {
			return AuthService.register(credentials)
				.then(() => {
					commit('REGISTER_SUCCESS')
				})
				.catch((error) => {
					commit('REGISTER_FAILURE')
					throw new Error(error)
				})
		},
		logout ({commit}) {
			AuthService.logout()
			commit('LOGOUT')
		}
	},
	mutations: {
		LOGIN_SUCCESS (state) {
			state.token = AuthService.getToken()
			state.isAuth = true
		},
		LOGIN_FAILURE (state) {
			state.token = null
			state.isAuth = false
		},
		REGISTER_SUCCESS (state) {
			state.isAuth = false
		},
		REGISTER_FAILURE (state) {
			state.isAuth = false
		},
		LOGOUT (state) {
			state.token = null
			state.isAuth = false
		}
	},
	getters: {
		isAuth: state => state.isAuth,
		token: state => state.token,
		isAdmin: state => state.isAuth ? jwtDecode(state.token).user_claims.admin : false,
		isGuest: state => !state.isAuth,
		userInfos: state => state.isAuth ? jwtDecode(state.token).user_claims : null,
	}
}
