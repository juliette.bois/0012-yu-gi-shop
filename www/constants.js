export default {
	// ENDPOINT
	API_ENDPOINT: 'http://localhost:5000/api',

	// SESSION
	AUTH_TOKEN_KEY: 'auth-token',

	// ROUTE PATH
	HOME_PATH: '/',

	// ROUTE NAMES
	LOGIN_ROUTE_NAME: 'Login',
	HOME_ROUTE_NAME: 'Home',

	// SCOPES
	AUTH_SCOPE: 'auth',
	ADMIN_SCOPE: 'admin',
	GUEST_SCOPE: 'guest'
}
