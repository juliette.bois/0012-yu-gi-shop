/**
 *
 * @param {Object} card
 * @returns {*}
 */
export function getId (card) {
	return card._id.$oid
}
