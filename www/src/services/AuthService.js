import Service from './Service'
import ApiService from './ApiService'
import constants from '../../constants'

export default class AuthService extends Service {
	constructor () {
		super()
	}

	static login (credentials) {
		return ApiService.request({
			url: '/auth/login',
			method: 'post',
			data: credentials
		})
			.then((response) => {
				let token = response.data.token
				if (token) {
					localStorage.setItem(constants.AUTH_TOKEN_KEY, token)
				}

				return response
			})
	}

	static logout () {
		localStorage.removeItem(constants.AUTH_TOKEN_KEY)
	}

	static register (credentials) {
		return ApiService.request({
			url: '/auth/signup',
			method: 'post',
			data: credentials
		})
	}

	static getToken () {
		return localStorage.getItem(constants.AUTH_TOKEN_KEY)
	}

	static setToken (token) {
		localStorage.setItem(constants.AUTH_TOKEN_KEY, token)
	}
}
