/* eslint-disable */
import Service from './Service'
import axios from 'axios'
import constants from '../../constants'

export default class ApiService extends Service {
	constructor () {
		super()
	}

	static getApiEndpoint () {
		return constants.API_ENDPOINT
	}

	/**
	 * Make HTTP request
	 * @param {Object}options ('url', 'method', 'headers', data
	 * @returns {AxiosPromise}
	 */
	static async request (options) {
		options.url = `${this.getApiEndpoint()}${options.url}`

		return axios(options).then(response => response.data)
	}
}
