// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import HeaderApp from './components/partials/Header'
import FooterApp from './components/partials/Footer'
import router from './router'
import store from '../store'

Vue.component('header-app', HeaderApp)
Vue.component('footer-app', FooterApp)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
	el: '#app',
	store,
	router,
	components: {App, HeaderApp, FooterApp},
	template: '<App/>'
})
