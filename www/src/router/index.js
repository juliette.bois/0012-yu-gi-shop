import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Card from '@/components/Card'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Create from '@/components/Create'
import constants from '../../constants'
import store from '../../store'

const {AUTH_SCOPE, ADMIN_SCOPE, GUEST_SCOPE, LOGIN_ROUTE_NAME, HOME_ROUTE_NAME} = constants

Vue.use(Router)

const router = new Router({
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home
		},
		{
			path: '/card/:id',
			name: 'Card',
			component: Card,
			props: true
		},
		{
			path: '/login',
			name: 'Login',
			component: Login,
			meta: {
				scope: [GUEST_SCOPE]
			}
		},
		{
			path: '/register',
			name: 'Register',
			component: Register,
			meta: {
				scope: [GUEST_SCOPE]
			}
		},
		{
			path: '/create',
			name: 'Create',
			component: Create,
			meta: {
				scope: [ADMIN_SCOPE]
			}
		},
		{
			path: '*',
			redirect: '/'
		}
	]
})

router.beforeEach((to, from, next) => {
	if (to.hasOwnProperty('meta') && to.meta.hasOwnProperty('scope')) {
		if (to.meta.scope.includes(ADMIN_SCOPE) && !store.getters.isAdmin) {
			next(LOGIN_ROUTE_NAME)
		} else if (to.meta.scope.includes(AUTH_SCOPE) && !store.getters.isAuth) {
			next(LOGIN_ROUTE_NAME)
		} else if (to.meta.scope.includes(GUEST_SCOPE) && !store.getters.isGuest) {
			next(HOME_ROUTE_NAME)
		} else {
			next()
		}
	} else {
		next()
	}
})

export default router
