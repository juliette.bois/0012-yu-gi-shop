# API avec MongoDB

Voici les commandes à exécuter dans l'ordre après avoir cloner le projet, dans `yu-gi-shop/api`

Déjà, il faut installer pipenv. Pipenv est utilisé pour créer un environnement virtuel qui isole les packages python que tu utilises dans ce projet des autres packages python système.
```pip install --user pipenv```

Puis pour installer les dépendances
```pipenv install```

Ensuite, tu peux activer l'environnement virtuel que tu viens de créer
```pipenv shell```

Penses à exporter les variables d'environnement :
```export ENV_FILE_LOCATION=./.env```

Enfin, tu peux run l'application
```python3.7 run.py```

Dans un autre onglet du terminal, toujours dans yu-gi-shop/api, tu peux démarrer un serveur SMTP avec:
```python -m smtpd -n -c DebuggingServer localhost:1025```

## Les routes

##### 1. Pour se créer un compte
```
http://localhost:5000/api/auth/signup [POST]
{
    "email": "theo.champion@ipilyon.net",
    "username": "teh0"
    "password": "monmotdepassehypersecret"
}
```

##### 2. Pour se connecter
```
http://localhost:5000/api/auth/login [POST]
{
    "email": "theo.champion@ipilyon.net",
    "password": "monmotdepassehypersecret"
}

Ici, l'API te renverra un token. Copie le bien afin de l'ajouter dans l'en-tête d'autorisation de Postman. Choisis le type Bearer Token
```

##### 3. Pour voir toutes les cartes
```
http://localhost:5000/api/cards [GET]
```

##### 4. Pour voir une carte précise
```
http://localhost:5000/api/cards/id [GET]
```

##### 5. Pour ajouter une carte
```
http://localhost:5000/api/cards [POST]
{
    "name": "Soldat Galactique",
    "key": "WSUP-FR010",
    "description": "ici mettre une description",
    "amount": 1
}
```

##### 6. Pour modifier une carte
```
http://localhost:5000/api/cards/id [PUT]
{
    "name": "Soldat Galactique 2",
    "key": "WSUP-FR010",
    "description": "ici mettre une description",
    "amount": 1
}
```

##### 7. Pour supprimer une carte
```
http://localhost:5000/api/cards/id [DELETE]
```
