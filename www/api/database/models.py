from .db import db
from flask_bcrypt import generate_password_hash, \
    check_password_hash


class Card(db.Document):
    name = db.StringField(required=True, unique=True)
    key = db.StringField(required=True)
    image = db.StringField(required=True)
    amount = db.IntField(default=1)
    added_by = db.ReferenceField('User')
    description = db.StringField(required=True)


class User(db.Document):
    email = db.EmailField(required=True, unique=True)
    username = db.StringField(required=True)
    password = db.StringField(required=True, min_length=6)
    admin = db.BooleanField(default=False)

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf8')

    def check_password(self, password):
        return check_password_hash(self.password, password)


# if a user is deleted, the card created by the user is also deleted.
User.register_delete_rule(Card, 'added_by', db.CASCADE)
