from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from database.db import initialize_db
from flask_restful import Api
from resources.errors import errors
from flask_mail import Mail

# init app
from resources.routes import initialize_routes

app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')
mail = Mail(app)
app.config['MONGODB_SETTINGS'] = {
  'host': 'mongodb://localhost/api'
}
CORS(app)


# init api
api = Api(app, errors=errors)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)


# setup JWT
@jwt.user_claims_loader
def add_claim_role_to_access_token(user):
    return {'admin': user.admin, 'username': user.username}


@jwt.user_identity_loader
def user_identity_lookup(user):
    return str(user.id)


# trigger initialization
initialize_db(app)
initialize_routes(api)
