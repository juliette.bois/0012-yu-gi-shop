from flask import make_response, jsonify


def api_success_response(data, code=200):
    return make_response(jsonify(result="success", data=data), code)


def api_error_response(data, code=400):
    return make_response(jsonify(result="error", error=data), code)
