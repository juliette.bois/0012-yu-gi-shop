from .card import CardsApi, CardApi
from .auth import SignupApi, LoginApi


def initialize_routes(api):
    api.add_resource(CardsApi, '/api/cards')
    api.add_resource(CardApi, '/api/cards/<id>')
    api.add_resource(SignupApi, '/api/auth/signup')
    api.add_resource(LoginApi, '/api/auth/login')
