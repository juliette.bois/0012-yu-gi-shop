class InternalServerError(Exception):
    pass


class SchemaValidationError(Exception):
    pass


class CardAlreadyExistsError(Exception):
    pass


class UpdatingCardError(Exception):
    pass


class DeletingCardError(Exception):
    pass


class CardNotExistsError(Exception):
    pass


class EmailAlreadyExistsError(Exception):
    pass


class UnauthorizedError(Exception):
    pass


class EmailDoesnotExistsError(Exception):
    pass


class BadTokenError(Exception):
    pass


errors = {
    "InternalServerError": {
        "message": "Something went wrong",
        "status": 500
    },
    "SchemaValidationError": {
        "message": "Request is missing required fields",
        "status": 400
    },
    "CardAlreadyExistsError": {
        "message": "Card with given name already exists",
        "status": 400
    },
    "UpdatingCardError": {
        "message": "Updating card added by other is forbidden",
        "status": 403
    },
    "DeletingCardError": {
        "message": "Deleting card added by other is forbidden",
        "status": 403
    },
    "CardNotExistsError": {
        "message": "Card with given id doesn't exists",
        "status": 400
    },
    "EmailAlreadyExistsError": {
        "message": "User with given email address already exists",
        "status": 400
    },
    "UnauthorizedError": {
        "message": "Invalid username or password",
        "status": 401
    },
    "EmailDoesnotExistsError": {
         "message": "Couldn't find the user with given email address",
         "status": 400
    },
    "BadTokenError": {
         "message": "Invalid token",
         "status": 403
    }
}
