from flask import request, \
    jsonify, \
    make_response
from flask_jwt_extended import create_access_token

from auth.middlewares import guest_required
from database.models import User
from flask_restful import Resource
import datetime
from mongoengine.errors import FieldDoesNotExist, \
    NotUniqueError, \
    DoesNotExist
from resources.errors import SchemaValidationError, \
    EmailAlreadyExistsError, \
    UnauthorizedError, \
    InternalServerError


class SignupApi(Resource):
    @guest_required
    def post(self):
        try:
            body = request.get_json()
            user = User(**body)
            user.hash_password()
            user.admin = False
            user.save()
            return make_response(jsonify(result='success'), 200)

        except FieldDoesNotExist:
            raise SchemaValidationError
        except NotUniqueError:
            raise EmailAlreadyExistsError
        except Exception as e:
            raise InternalServerError


class LoginApi(Resource):
    @guest_required
    def post(self):
        try:
            body = request.get_json()
            user = User.objects.get(email=body.get('email'))
            authorized = user.check_password(body.get('password'))
            if not authorized:
                raise UnauthorizedError

            expires = datetime.timedelta(days=7)
            access_token = create_access_token(identity=user, expires_delta=expires)
            token = {'token': access_token}
            return make_response(jsonify(result='success', data=token), 200)

        except (UnauthorizedError, DoesNotExist):
            raise UnauthorizedError
        except Exception as e:
            raise InternalServerError
