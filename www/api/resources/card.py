import os

from flask import Response, request
from flask_jwt_extended import get_jwt_identity
from flask_restful import Resource
from mongoengine.errors import FieldDoesNotExist, \
    NotUniqueError, \
    DoesNotExist, \
    ValidationError, \
    InvalidQueryError

from auth.middlewares import auth_required
from database.models import Card, User
from definition import UPLOAD_DIR
from resources.errors import SchemaValidationError, \
    CardAlreadyExistsError, InternalServerError, \
    UpdatingCardError, \
    DeletingCardError, \
    CardNotExistsError


class CardsApi(Resource):
    def get(self):
        cards = Card.objects().to_json()
        return Response(
            cards,
            mimetype="application/json",
            status=200)

    @auth_required
    def post(self):
        try:
            user_id = get_jwt_identity()
            data = request.form
            user = User.objects.get(id=user_id)
            image = request.files['card_image']
            image_name = image.filename
            print(os.path.join(UPLOAD_DIR, image_name))
            image.save(os.path.join(UPLOAD_DIR, image_name))
            Card(name=data['name'],
                 key=data['key'],
                 amount=data['amount'],
                 image=image_name,
                 description=data['description'],
                 added_by=user_id).save()

            return {'result': 'success'}, 200

        except (FieldDoesNotExist, ValidationError):
            raise SchemaValidationError
        except NotUniqueError:
            raise CardAlreadyExistsError
        except Exception as e:
            raise InternalServerError


class CardApi(Resource):
    @auth_required
    def put(self, id):
        try:
            data = request.form
            card = Card.objects.get(id=id)
            card.name = data['name']
            card.key = data['key']
            card.amount = data['amount']
            card.description = data['description']
            # card.image = image.filename
            card.save()

            return 'Card has been changed', 200

        except InvalidQueryError:
            raise SchemaValidationError
        except DoesNotExist:
            raise UpdatingCardError
        except Exception:
            raise InternalServerError

    @auth_required
    def delete(self, id):
        try:
            user_id = get_jwt_identity()
            card = Card.objects.get(id=id, added_by=user_id)
            card.delete()
            return 'Card has been deleted', 200
        except DoesNotExist:
            raise DeletingCardError
        except Exception:
            raise InternalServerError

    def get(self, id):
        try:
            cards = Card.objects.get(id=id).to_json()
            return Response(
                cards,
                mimetype="application/json",
                status=200)
        except DoesNotExist:
            raise CardNotExistsError
        except Exception:
            raise InternalServerError
