# custom decorator for auth
from functools import wraps
from flask_jwt_extended import verify_jwt_in_request, \
    get_jwt_claims
from helpers.response import api_error_response


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if not claims['admin']:
            return api_error_response('Only Admin')
        else:
            return fn(*args, **kwargs)

    return wrapper


def guest_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            verify_jwt_in_request()
            return api_error_response('Only Guest')
        except:
            return fn(*args, **kwargs)

    return wrapper


def auth_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            verify_jwt_in_request()
        except Exception as e:
            return api_error_response('Auth Only')

        return fn(*args, **kwargs)

    return wrapper
