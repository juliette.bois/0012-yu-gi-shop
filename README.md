# Yu-Gi-Shop

Projet scolaire en bachelor (2019)

### 1. Présentation du projet Python
Nous avons fait le choix de créer une boutique en ligne de cartes Yu-Gi-Oh avec :
- Une page pour lister les cartes
- Une page descriptive d'une carte
- Une page pour se connecter
- Une page pour se créer un compte
- Un dashboard admin pour créer des cartes


Nous avons veillés à :
- respecter le guide de style (PEP8)
- la simplicité et la lisibilité du code

Nous avons malgré tout utilisé un framework CSS ([Tailwind](https://tailwindcss.com/)) pour avoir un front le plus agréable possible. 
En revanche, le responsive n'a pas été fait car nous avons préféré nous concentrer sur le développement du back-end en python.
Nous nous sommes servis de Vue.js pour construire notre front.

Les fonctionnalités que nous proposons sur Yu-Gi-Shop :
- Système d'authentification avec Connexion et Inscription
- Système de droits utilisateur avec des utilisateurs simples et des administrateurs
- Les utilisateurs simples peuvent seulement voir la liste des cartes et la fiche descriptive d'une carte en particulier
- Les administrateurs peuvent en plus créer des cartes, les modifier ou les supprimer, gérer la quantité/stock


### 2. Le back-end
Nous vous proposons un readme.md dans `www/api` afin de vous aider à mettre en place le back-end.
